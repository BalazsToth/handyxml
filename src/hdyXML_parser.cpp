/*
    Copyright 2016-2017 Balazs Toth
    This file is part of HandyXML.

    HandyXML is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HandyXML is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with HandyXML.  If not, see <http://www.gnu.org/licenses/>.

    For more information please visit: https://bitbucket.org/HandyXMLproject/
*/

#include "hdyXML_parser.h"
#include "prolog/pLogger.h"
#include "commonutils/Common.h"

using namespace HandyXML;

/////////////////////////////////////////////////////////////////////////////////////////
/// Copy contructor.
/////////////////////////////////////////////////////////////////////////////////////////
hdyXML_parser::pmBlock::pmBlock(pmBlock const& other) {
	this->level = other.level;
	for(auto const& it:child) {
		this->child.push_back(std::make_shared<pmBlock>(*it));
	}
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Move constructor;
/////////////////////////////////////////////////////////////////////////////////////////
hdyXML_parser::pmBlock::pmBlock(pmBlock&& other) {
	this->level = std::move(other.level);
	this->child = std::move(other.child);
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Copy assignment operator.
/////////////////////////////////////////////////////////////////////////////////////////
hdyXML_parser::pmBlock& hdyXML_parser::pmBlock::operator=(pmBlock const& other) {
	if(this!=&other) {
		this->level = other.level;
		for(auto const& it:child) {
			this->child.push_back(std::make_shared<pmBlock>(*it));
		}
	}
	return *this;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Move assignment operator.
/////////////////////////////////////////////////////////////////////////////////////////
hdyXML_parser::pmBlock& hdyXML_parser::pmBlock::operator=(pmBlock&& other) {
	if(this!=&other) {
		this->level = std::move(other.level);
		this->child = std::move(other.child);
	}
	return *this;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Prints the content of the block.
/////////////////////////////////////////////////////////////////////////////////////////
void hdyXML_parser::pmBlock::print() const {
	std::cout << name << std::endl;
	for(auto it:child) {
		for(int j=0;j<level;j++) std::cout<< "  ";
		it->print();
	}
	std::cout << std::endl;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Adds a child to the block in the tree.
/////////////////////////////////////////////////////////////////////////////////////////
void hdyXML_parser::pmBlock::add_child(std::shared_ptr<hdyXML_parser::pmBlock> b) {
	child.push_back(b);
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Returns the level occupied by the block.
/////////////////////////////////////////////////////////////////////////////////////////
int hdyXML_parser::pmBlock::get_level() const {
	return level;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Returns the name of the block or entry.
/////////////////////////////////////////////////////////////////////////////////////////
std::string hdyXML_parser::pmBlock::get_name() const {
	return name;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Returns the std::vector of the blocks found in the XML tree.
/////////////////////////////////////////////////////////////////////////////////////////
std::vector<std::shared_ptr<hdyXML_parser::pmBlock>> hdyXML_parser::pmBlock::find_block(std::string const& blockname) {
	return find_block(blockname, true);
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Finds blocks with the given name recursively in the XML tree and wraps them in an
/// std::vector.
/////////////////////////////////////////////////////////////////////////////////////////
std::vector<std::shared_ptr<hdyXML_parser::pmBlock>> hdyXML_parser::pmBlock::find_block(std::string const& blockname, bool reset) {
	static std::vector<std::shared_ptr<hdyXML_parser::pmBlock>> found;
	if(reset) found.clear();
	if(this->name==blockname) {
		std::shared_ptr<pmBlock> sp_this = shared_from_this();
		found.push_back(sp_this);
		return found;
	} else {
		for(auto i:this->child) {
			i->find_block(blockname, false);
		}
		return found;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Returns the value of the block with the given name.
/////////////////////////////////////////////////////////////////////////////////////////
std::vector<std::shared_ptr<hdyXML_parser::pmEntry>> hdyXML_parser::pmBlock::get_entry(std::string const& n) const {
	std::vector<std::shared_ptr<pmEntry>> entry;
	for(auto const& it:child) {
		std::shared_ptr<pmEntry> e = std::dynamic_pointer_cast<pmEntry>(it);
		if(e.use_count()!=0) {
			if(e->get_name()==n) {
				entry.push_back(e);
			}
		}
	}
	return entry;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Returns the children of the block.
/////////////////////////////////////////////////////////////////////////////////////////
std::vector<std::shared_ptr<hdyXML_parser::pmBlock>> hdyXML_parser::pmBlock::get_children() const {
	return child;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Copy contructor.
/////////////////////////////////////////////////////////////////////////////////////////
hdyXML_parser::pmEntry::pmEntry(pmEntry const& other) {
	this->valuename = other.valuename;
	this->value = other.value;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Move constructor;
/////////////////////////////////////////////////////////////////////////////////////////
hdyXML_parser::pmEntry::pmEntry(pmEntry&& other) {
	this->valuename = std::move(other.valuename);
	this->value = std::move(other.value);
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Copy assignment operator.
/////////////////////////////////////////////////////////////////////////////////////////
hdyXML_parser::pmEntry& hdyXML_parser::pmEntry::operator=(pmEntry const& other) {
	if(this!=&other) {
		this->valuename = other.valuename;
		this->value = other.value;
	}
	return *this;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Move assignment operator.
/////////////////////////////////////////////////////////////////////////////////////////
hdyXML_parser::pmEntry& hdyXML_parser::pmEntry::operator=(pmEntry&& other) {
	if(this!=&other) {
		this->valuename = std::move(other.valuename);
		this->value = std::move(other.value);
	}
	return *this;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Prints the constent of the entry
/////////////////////////////////////////////////////////////////////////////////////////
void  hdyXML_parser::pmEntry::print() const {
	std::cout << name << ": ";
	for(int i=0; i<value.size(); i++) {
		std::cout << valuename[i] << "=" << value[i] << ' ';
	}
	std::cout << std::endl;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Returns the value of the entry with the given name.
/////////////////////////////////////////////////////////////////////////////////////////
std::string hdyXML_parser::pmBlock::get_value(std::string const& n) const {
	for(int i=0; i<valuename.size(); i++) {
		if(valuename[i]==n) {
			return value[i];
		}
	}
	return "";
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Reads the XML file.
/////////////////////////////////////////////////////////////////////////////////////////
void hdyXML_parser::read_file_content(std::string const& filename) {
	file.open(filename.c_str());
	if(file.fail()) throw(false);
	file >> std::ws;	// eat white spaces
	char header[100];
	file.getline(header,100); // skip header
	bool inentryname = false;
	while(!file.eof()) {
		char ch;
		char sp = file.peek();
		file >> std::ws;	// eat white spaces
		file.get(ch);
		// Keep one space if we are right after an entry name
		if(ch=='<') {
			inentryname = true;
		} else if(sp==' ' && inentryname == true) {
			inentryname = false;
			filecontent += ' ';
		}

		if(!file.eof())
			filecontent += ch;
	}
	file.close();
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Removes the comments in the file content
/////////////////////////////////////////////////////////////////////////////////////////
void hdyXML_parser::remove_comments() {
	std::string start = "<!--";
	std::string end = "-->";

	std::vector<int> cstart = Common::find_word(filecontent, start);
	std::vector<int> cend = Common::find_word(filecontent, end);

	if(cstart.size()!=cend.size()) {
		std::cerr << "invalid comments!" << std::endl;
	}

	int shifting = 0;
	for(int i=0; i<cstart.size(); i++) {
		filecontent.erase(cstart[i]-shifting, cend[i]-cstart[i]+end.size());
		shifting+=cend[i]-cstart[i]+end.size();
	}
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Returns the name of the case.
/////////////////////////////////////////////////////////////////////////////////////////
std::string hdyXML_parser::get_XML_name() {
	std::vector<int> idx = Common::find_word(filecontent,"name");
	if(idx.empty()) return "default";
	std::string name = filecontent.substr(idx[0]+5,idx[1]-idx[0]-7);
	filecontent.erase(idx[0]-1,idx[1]-idx[0]+6);
	return name;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Returns the name of the block given in XML format.
/////////////////////////////////////////////////////////////////////////////////////////
std::string hdyXML_parser::get_block_name(std::string const& content) const {
	std::string elementname;
	int i=content[0]=='<'?1:0;
	while(content[i]!='>' && content[i]!=' ') {
		elementname += content[i];
		i++;
	}
	return elementname;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Returns the name of the entry given in XML format.
/////////////////////////////////////////////////////////////////////////////////////////
void hdyXML_parser::read_entry_value(std::string::iterator& it, std::shared_ptr<pmEntry> ent) {
	std::string vname;
	while(*it!='=') {
		vname += *it;
		it++;
	}
	ent->valuename.push_back(vname);
	it++;
	it++;
	std::string value;
	while(*it!='"') {
		value += *it;
		it++;
	}
	ent->value.push_back(value);
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Reads an entry and pushes to the XML tree.
/////////////////////////////////////////////////////////////////////////////////////////
void hdyXML_parser::read_entry(std::string content, std::shared_ptr<pmEntry> entry) {
	std::string bname = get_block_name(content);
	std::string::iterator it = content.begin()+bname.size()+1;
	while(it!=content.end()) {
		read_entry_value(it, entry);
		it++;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Reads a block recursively and pushes to the XML tree.
/////////////////////////////////////////////////////////////////////////////////////////
std::shared_ptr<hdyXML_parser::pmBlock> hdyXML_parser::read_block(std::string content) {
	std::string blockname = get_block_name(content);
	std::shared_ptr<pmBlock> parent = std::make_shared<pmBlock>(blockname, 1);
	int start = 1;
	std::vector<int> end = Common::find_word(content, "/"+blockname);
	std::string blockcontent = content.substr(start+blockname.size()+1, end[0]-blockname.size()-4);
	read_block(blockcontent, parent);
	return parent;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Reads a block and pushes to the XML tree.
/////////////////////////////////////////////////////////////////////////////////////////
void hdyXML_parser::read_block(std::string content, std::shared_ptr<pmBlock> parent) {
	while(!content.empty()) {
		std::string blockname = get_block_name(content);
		int start = 1;
		std::vector<int> end = Common::find_word(content, "</"+blockname);
		std::shared_ptr<pmBlock> child;

		if(!end.empty()) { // block
			child = std::make_shared<pmBlock>(blockname, parent->get_level()+1);
			std::string blockcontent = content.substr(start+blockname.size()+1, end[0]-blockname.size()-2);
			content.erase(start-1, end[0]-start+blockname.size()+4);
			if(!blockcontent.empty()) {
				read_block(blockcontent, child);
			}
		} else { // entry
			child = std::make_shared<pmEntry>(blockname, parent->get_level()+1);
			std::vector<int> entry_end = Common::find_word(content, "/>");
			std::string entrycontent = content.substr(start, entry_end[0]-1);
			content.erase(start-1, entry_end[0]-start+3);
			read_entry(entrycontent, std::dynamic_pointer_cast<pmEntry>(child));
		}
		parent->add_child(child);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Verifies the XML format.
/////////////////////////////////////////////////////////////////////////////////////////
bool hdyXML_parser::verify() const {
	std::vector<int> open = Common::find_word(filecontent, "<");
	std::vector<int> close = Common::find_word(filecontent, ">");
	if(open.size()!=close.size()) {
		throw false;
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Reads the file with given name and builds the XML tree.
/////////////////////////////////////////////////////////////////////////////////////////
void hdyXML_parser::read_file(std::string const& filename) {
	try {
		read_file_content(filename);
		// verify();
		remove_comments();
		name = get_XML_name();
		block = read_block(filecontent);
	} catch(...) {
		ProLog::pLogger::error_msgf("\"%s\" file is missing or corrupt.\n", filename.c_str());
	}
	ProLog::pLogger::footer<ProLog::LCY>();
	ProLog::pLogger::logf<ProLog::LCY>("  XML file is loaded successfully.\n");
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Prints the entire XML tree.
/////////////////////////////////////////////////////////////////////////////////////////
void hdyXML_parser::print() const {
	block->print();
}

/////////////////////////////////////////////////////////////////////////////////////////
/// Adds new block with the given name to parent.
/////////////////////////////////////////////////////////////////////////////////////////
void hdyXML_parser::add_block(std::shared_ptr<pmBlock> parent, std::string const& name) const {
	std::shared_ptr<pmBlock> child = std::make_shared<pmBlock>(name, parent->get_level()+1);
	parent->add_child(child);
}
