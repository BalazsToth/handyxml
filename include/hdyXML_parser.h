/*
    Copyright 2016-2017 Balazs Toth
    This file is part of HandyXML.

    HandyXML is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    HandyXML is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with HandyXML.  If not, see <http://www.gnu.org/licenses/>.

    For more information please visit: https://bitbucket.org/HandyXMLproject/
*/

#ifndef _XML_PARSER_H_
#define _XML_PARSER_H_

#include <fstream>
#include <vector>
#include <memory>
#include <string>

namespace HandyXML {

	/**	This class realizes the parsing process of an XML formatted file. After
	//	the analysis it holds a tree of blocks and entries with all information.
	*/
	class hdyXML_parser {
		std::fstream file;
		std::string name;
		std::string filecontent;
	protected:
		class pmEntry;
		class pmBlock : public std::enable_shared_from_this<pmBlock> {
		public:
			std::vector<std::string> valuename;
			std::vector<std::string> value;
		private:
			std::vector<std::shared_ptr<pmBlock>> child;
		protected:
			int level;
			std::string name;
		public:
			pmBlock() {}
			pmBlock(std::string n, int lv) : name{n}, level{lv} {}
			pmBlock(pmBlock const& other);
			pmBlock(pmBlock&& other);
			pmBlock& operator=(pmBlock const& other);
			pmBlock& operator=(pmBlock&& other);
			virtual ~pmBlock() {}
			virtual	void print() const;
			void add_child(std::shared_ptr<hdyXML_parser::pmBlock> b);
			int get_level() const;
			std::vector<std::shared_ptr<pmBlock>> find_block(std::string const& blockname);
			std::vector<std::shared_ptr<pmBlock>> find_block(std::string const& blockname, bool reset);
			std::vector<std::shared_ptr<pmEntry>> get_entry(std::string const&) const;
			std::vector<std::shared_ptr<pmBlock>> get_children() const;
			std::string get_name() const;
			std::string get_value(std::string const& n) const;
		};
		class pmEntry final : public pmBlock {
		public:
			pmEntry() {}
			pmEntry(std::string n, int lv) : pmBlock{n,lv} {}
			pmEntry(pmEntry const& other);
			pmEntry(pmEntry&& other);
			pmEntry& operator=(pmEntry const& other);
			pmEntry& operator=(pmEntry&& other);
			~pmEntry() override {}
			void print() const override;
		};

		void read_file_content(std::string const& n);
		void remove_comments();
		std::string get_XML_name();
		std::string get_block_name(std::string const& n) const;
		void read_entry_value(std::string::iterator& it, std::shared_ptr<pmEntry> entry);
		void read_entry(std::string n, std::shared_ptr<pmEntry> entry);
		std::shared_ptr<pmBlock> read_block(std::string content);
		void read_block(std::string n, std::shared_ptr<pmBlock> parent);
		bool verify() const;
	public:
		std::shared_ptr<pmBlock> block = std::make_shared<pmBlock>();
		hdyXML_parser() {}
		hdyXML_parser(hdyXML_parser const&)=delete;
		hdyXML_parser(hdyXML_parser&&)=delete;
		hdyXML_parser& operator=(hdyXML_parser const&)=delete;
		hdyXML_parser& operator=(hdyXML_parser&&)=delete;
		virtual ~hdyXML_parser() {}
		void read_file(std::string const& filename);
		void print() const;
		void add_block(std::shared_ptr<pmBlock> parent, std::string const& name) const;
	};
}

#endif //_XML_PARSER_H_